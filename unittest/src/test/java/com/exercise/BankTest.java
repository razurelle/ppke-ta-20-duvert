package com.exercise;

import com.example.cart.Cart;
import com.example.cart.ListItem;
import com.example.user.UserEmailNotVerifiedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
class BankTest {
    private Bank bank = new Bank();
    private Account from;
    private Account to = new Account();
    private Bank spy = spy(bank);

    @BeforeEach
    public void initBank() {
        from = new Account();
        from.addTransaction(new Transaction("test", 10000));
    }

    @Test
    public void testTransfer(@Mock LedgerService ledgerService) throws NotAllowedException {
        var bank = new Bank(ledgerService);
        assertAll("Value before ",
                () -> assertEquals(from.getBalance(), 10000),
                () -> assertEquals(to.getBalance(), 0));
        bank.transfer(from, to, 10000);
        assertAll("Value before ",
                () -> assertEquals(from.getBalance(), 0),
                () -> assertEquals(to.getBalance(), 10000));
    }
//verify structure called with verify->never
    @Test
    public void testTransfer_whenBalanceIsNotEnough() {
        var exception = assertThrows(
                NotAllowedException.class,
                () -> bank.transfer(from, to, 13000));
        assertEquals("Account is not eligible for transfer for amount 13000", exception.getMessage());
    }

    @Test
    public void testTransfer_whenLedgerServiceThrow(@Mock LedgerService ledgerService) throws NotAllowedException {
        var spy = spy(bank);
        doNothing().when(spy).transfer(from,to,100);
        // Original call
        assertThrows(
                RuntimeException.class,
                ()->{bank.transfer(from,to,100);});
        // Spy
        assertDoesNotThrow(()->{spy.transfer(from,to,100);});
        //assertEquals(1, cart.getAll().size());
        //assertEquals(1, spy.getAll().size());
        // Mock
        //mockedCart.add(book);
        //assertEquals(0, mockedCart.getAll().size());
    }
}