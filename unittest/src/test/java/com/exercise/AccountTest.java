package com.exercise;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Unit
public class AccountTest {
    @Test
    public void testAddTransaction() {
        var account = new Account();
        account.addTransaction(new Transaction("first",0));
        account.addTransaction(new Transaction("second",0));
        var transactions = account.getTransactions();
        assertEquals(2, transactions.size());
    }

    @Test
    public void testBalanceFromTransactions() {
        int a = 0;
        var b = new Random();
        var n = 0;
        var c = new Account();

        assertEquals(0, c.getBalance());
        for (int i = 0; i < 5; i++) {
            n = b.nextInt(1000);
            a += n;
            c.addTransaction(new Transaction(String.valueOf(i), n));
        }
        assertEquals(a, c.getBalance());
    }
}
