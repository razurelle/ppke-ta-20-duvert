package com.epam.university.homework.pagesModels.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CompletionFormPage extends BasePage {
    private By ConfirmationIcon = By.xpath("//div[@class='thank-you-page-confirm']//i");
    public CompletionFormPage(WebDriver driver) { super(driver); }

    public boolean isIconPresent() {
        return driver.findElement(ConfirmationIcon).isDisplayed();
    }

}
