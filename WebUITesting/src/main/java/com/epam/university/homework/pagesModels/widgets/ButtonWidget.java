package com.epam.university.homework.pagesModels.widgets;

import com.epam.university.homework.pagesModels.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ButtonWidget extends BasePage {
    private By widget;

    public ButtonWidget(WebDriver driver, By _widget) {
        super(driver);
        widget = _widget;
    }

    public WebElement button() {return driver.findElement(widget); }

    public boolean isButtonClickable() {
        return driver.findElement(widget).isEnabled();
    }

    public WebDriver clickOnButton() {
        driver.findElement(widget).click();
        return driver;
    }
}
