package com.epam.university.homework.pagesModels.pages;

import org.openqa.selenium.WebDriver;

public class BasePage {

    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public void navigateToForm(WebDriver driver) {
        driver.get("https://forms.office.com/Pages/ResponsePage.aspx?id=0HIbtJ9OJkyKaflJ82fJHVojrusiOT1BhHKIbJ19HYJUNUc3RENVWFdaWTBJTVo3Mkk0SFVRWFFINS4u");
    }
}