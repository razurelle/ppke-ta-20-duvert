package com.epam.university.homework.pagesModels.widgets;

import com.epam.university.homework.pagesModels.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class InputTextFieldWidget extends BasePage {
    public InputTextFieldWidget(WebDriver driver) { super(driver); }

    public String GetTextInput(By field) {
        return driver.findElement(field).getAttribute("value");
    }

    public void SetTextIntoInput(By field, String text) {
        WebElement inputField = driver.findElement(field);
        inputField.sendKeys(text);
    }
}
