package com.epam.university.homework.pagesModels.widgets;

import com.epam.university.homework.pagesModels.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class MatrixOptionBoxWidget extends BasePage {
    public MatrixOptionBoxWidget(WebDriver driver) {
        super(driver);
    }

    public ArrayList<String> getAllOptionsBox(String firstQuestion, int size, boolean header) {
        ArrayList<String> matrixOutput = new ArrayList<>();
        int i = header?1:0;
        int y = 0;
        OptionBoxWidget box = new OptionBoxWidget(driver);
        while (y < size) {
            matrixOutput.add(box.getOptionChosen(By.xpath(firstQuestion +"[" + i + "]//input")));
            i++;
            y++;
        }
        return matrixOutput;
    }

    public void fillAllOptionsBox(String firstQuestion, ArrayList<Integer> matrixInput, int size, boolean header) {
        int i = header?1:0;
        int y = 0;
        OptionBoxWidget box = new OptionBoxWidget(driver);
        while (y < size) {
            box.setOptionFromBy(By.xpath(firstQuestion +"[" + i + "]//input"), matrixInput.get(y));
            // box.setOptionFromWebElement(driver.findElements(By.xpath(firstQuestion +"[" + i + "]//input")).get(i), matrixInput.get(y));
            i++;
            y++;
        }
    }
}
