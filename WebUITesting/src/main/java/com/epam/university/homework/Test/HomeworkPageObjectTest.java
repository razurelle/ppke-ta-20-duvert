package com.epam.university.homework.Test;

import com.epam.university.homework.pagesModels.pages.CompletionFormPage;
import com.epam.university.homework.pagesModels.pages.EvaluationFormPage;
import com.epam.university.homework.pagesModels.pages.ExpectationFormPage;
import com.epam.university.homework.pagesModels.widgets.ButtonWidget;
import com.epam.university.homework.pagesModels.widgets.OptionBoxWidget;
import com.epam.university.homework.pagesModels.widgets.InputTextFieldWidget;
import com.epam.university.tests.TestBase;
import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RunWith(JUnitParamsRunner.class)
public class HomeworkPageObjectTest extends TestBase {
    private By QuestionTextBox = By.xpath("//div[@class='__question__ office-form-question  ']//input");
    private By MultipleBoxInput = By.xpath("//div[@class='__question__ office-form-question  '][2]//input");

    @Test
    public void checkFormFirstPageTest() throws IOException {
        // Navigate to first form page
        ExpectationFormPage page = new ExpectationFormPage(driver);
        page.navigateToForm(driver);
        // Get title page
        String title = page.getTitlePage();
        // Check number of question element
        Integer elementNb = page.getNumberQuestions();
        // Set question Input
        page.fillPage("test", 3);
        // get each Input
        ArrayList<String> Elements = page.getInputText();
        // Get Button to next page form
        ButtonWidget button = page.getNextPageButton();
        // checking all value
        try {
            Assert.assertEquals("Expectations", title);
            Assert.assertEquals(2, elementNb.intValue());
            Assert.assertEquals("test", Elements.get(0));
            Assert.assertEquals("3", Elements.get(1));
            Assert.assertTrue(button.isButtonClickable());
        } catch (AssertionError assertionError) {
            this.takeScreenshot("ExpectationsFormPage_Error");
        }
    }

    @Test
    public void checkFormSecondPageTest() throws IOException, InterruptedException {
        // Navigate to first form page then to the second one
        ExpectationFormPage page = new ExpectationFormPage(driver);
        page.navigateToForm(driver);
        page.fillPage("test", 3);
        EvaluationFormPage eval = new EvaluationFormPage(page.getNextPageButton().clickOnButton());
        // Wait for page to load
        //Thread.sleep(6000);
        //driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
        // Get title page
        String title = eval.getTitle();
        // Check number of question element
        Integer elementNb = eval.getNumberQuestions();
        // Set question Input
        ArrayList<Integer> matrixInput = new ArrayList<>();
        matrixInput.add(1);
        matrixInput.add(2);
        matrixInput.add(3);
        matrixInput.add(4);
        eval.fillQuestions(matrixInput, 2, "test", 5);
        // get each Input
        ArrayList<String> pageInputs = eval.getQuestionsInput();
        // Get Button to next page form and previous page
        ButtonWidget buttonNext = eval.getNextPageButton();
        ButtonWidget buttonPrevious = eval.getPrevPageButton();
        // checking all value
        buttonNext.clickOnButton();
        Thread.sleep(6000);
        try {
            Assert.assertEquals("Course evaluation", title);
            Assert.assertEquals(2, elementNb.intValue());
            Assert.assertEquals("2", pageInputs.get(0));
            Assert.assertEquals("3", pageInputs.get(1));
            Assert.assertEquals("4", pageInputs.get(2));
            Assert.assertEquals("5", pageInputs.get(3));
            Assert.assertEquals("2", pageInputs.get(4));
            Assert.assertEquals("test", pageInputs.get(5));
            Assert.assertEquals("5", pageInputs.get(6));
            Assert.assertTrue(buttonNext.isButtonClickable());
            Assert.assertTrue(buttonPrevious.isButtonClickable());
        } catch (AssertionError assertionError) {
            this.takeScreenshot("EvaluationFormPage_Error");
        }
    }

    @Test
    @FileParameters("src/main/resources/tdd/testHomework.csv")
    public void formCompletionTest(String Expectation, String Score, String topics,
                                   String adapt, String opinion, String recommend) throws InterruptedException {
        //first page
        ExpectationFormPage page = new ExpectationFormPage(driver);
        page.navigateToForm(driver);
        wait.until(ExpectedConditions.elementToBeClickable(page.getNextPageButton().button()));
        // fill answers
        page.fillPage(Expectation, Integer.parseInt(Score));
        // second page
        EvaluationFormPage eval = new EvaluationFormPage(page.getNextPageButton().clickOnButton());
        wait.until(ExpectedConditions.elementToBeClickable(eval.getNextPageButton().button()));
        // fill answers
        ArrayList<Integer> matrix = new ArrayList<>();
        matrix.add(Integer.parseInt(topics.substring(0,1)));
        matrix.add(Integer.parseInt(topics.substring(1,2)));
        matrix.add(Integer.parseInt(topics.substring(2,3)));
        matrix.add(Integer.parseInt(topics.substring(3,4)));
        eval.fillQuestions(matrix, Integer.parseInt(adapt), opinion, Integer.parseInt(recommend));
        // Final page
        CompletionFormPage finalPage = new CompletionFormPage(eval.getNextPageButton().clickOnButton());
        driver.manage().timeouts(). pageLoadTimeout(5, TimeUnit.SECONDS);
        // Confirm final page icon
        Assert.assertTrue(finalPage.isIconPresent());
    }

}
