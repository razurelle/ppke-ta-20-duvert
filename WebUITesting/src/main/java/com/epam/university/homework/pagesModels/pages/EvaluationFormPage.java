package com.epam.university.homework.pagesModels.pages;

import com.epam.university.homework.pagesModels.widgets.ButtonWidget;
import com.epam.university.homework.pagesModels.widgets.InputTextFieldWidget;
import com.epam.university.homework.pagesModels.widgets.MatrixOptionBoxWidget;
import com.epam.university.homework.pagesModels.widgets.OptionBoxWidget;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class EvaluationFormPage extends BasePage {
    private By Title = By.xpath("//div[@class='form-renderer-section-title']//span");
    private By QuestionList = By.xpath("//div[@class='__question__ office-form-question  '][0]");
    private String firstQuestion = "//div[@class='__question__ office-form-question  '][1]//div[@class='office-form-matrix-row']";
    private By secondQuestion = By.xpath("//div[@class='__question__ office-form-question  '][2]//input");
    private By thirdQuestion = By.xpath("//div[@class='__question__ office-form-question  '][3]//input");
    private By fourthQuestion = By.xpath("//div[@class='__question__ office-form-question  '][4]//td");
    private By NextPageButton = By.xpath("//div[@class='office-form-navigation-container']//button[2]");
    private By PrevPageButton = By.xpath("//div[@class='office-form-navigation-container']//button[1]");

    public EvaluationFormPage(WebDriver driver) { super(driver); }

    public String getTitle() { return driver.findElement(Title).getText(); }

    public int getNumberQuestions() {
        return driver.findElements(QuestionList).size();
    }

    public void fillQuestions(ArrayList<Integer> matrixInput, int input2, String input3, int input4) {
        MatrixOptionBoxWidget firstQuestionInput = new MatrixOptionBoxWidget(driver);
        OptionBoxWidget secondQuestionInput = new OptionBoxWidget(driver);
        InputTextFieldWidget thirdQuestionInput = new InputTextFieldWidget(driver);
        OptionBoxWidget fourthQuestionInput = new OptionBoxWidget(driver);

        firstQuestionInput.fillAllOptionsBox(firstQuestion, matrixInput, 4, true);
        secondQuestionInput.setOptionFromBy(secondQuestion, input2);
        thirdQuestionInput.SetTextIntoInput(thirdQuestion, input3);
        fourthQuestionInput.setOptionFromBy(fourthQuestion, input4);
    }

    public ArrayList<String> getQuestionsInput() {
        ArrayList<String> questions = new ArrayList<>();
        MatrixOptionBoxWidget firstQuestionInput = new MatrixOptionBoxWidget(driver);
        OptionBoxWidget secondQuestionInput = new OptionBoxWidget(driver);
        InputTextFieldWidget thirdQuestionInput = new InputTextFieldWidget(driver);
        OptionBoxWidget fourthQuestionInput = new OptionBoxWidget(driver);

        questions.addAll(firstQuestionInput.getAllOptionsBox(firstQuestion, 4, true));
        questions.add(secondQuestionInput.getOptionChosen(secondQuestion));
        questions.add(thirdQuestionInput.GetTextInput(thirdQuestion));
        questions.add(fourthQuestionInput.getOptionChosen(fourthQuestion));
        return questions;
    }

    public ButtonWidget getNextPageButton() {
        return new ButtonWidget(driver, NextPageButton);
    }
    public ButtonWidget getPrevPageButton() {
        return new ButtonWidget(driver, PrevPageButton);
    }
}