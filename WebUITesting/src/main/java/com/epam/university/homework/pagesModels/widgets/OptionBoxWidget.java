package com.epam.university.homework.pagesModels.widgets;

import com.epam.university.homework.pagesModels.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class OptionBoxWidget extends BasePage {
    public OptionBoxWidget(WebDriver driver) { super(driver); }

    public List<WebElement> getMultipleInput(By pos) {
        return driver.findElements(pos);
    }

    public String getOptionChosen(By widget) {
        int pos = -1;

        List<WebElement> elements = driver.findElements(widget);
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).getAttribute("value") != "-1") {
                pos = i + 1;
            }
        }
        return String.valueOf(pos);
    }

    public void setOptionFromBy(By Question, int QuestionInput) {
        driver.findElements(Question).get(QuestionInput).click();
    }
}
