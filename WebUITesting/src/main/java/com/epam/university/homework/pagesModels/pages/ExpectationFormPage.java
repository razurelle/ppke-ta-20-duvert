package com.epam.university.homework.pagesModels.pages;

import com.epam.university.homework.pagesModels.widgets.ButtonWidget;
import com.epam.university.homework.pagesModels.widgets.InputTextFieldWidget;
import com.epam.university.homework.pagesModels.widgets.OptionBoxWidget;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class ExpectationFormPage extends BasePage {
    private By Title = By.xpath("//div[@class='form-renderer-section-title']//span");
    private By QuestionList = By.xpath("//div[@class='__question__ office-form-question  '][0]");
    private By FirstQuestion = By.xpath("//div[@class='__question__ office-form-question  '][1]//input");
    private By SecondQuestion = By.xpath("//div[@class='__question__ office-form-question  '][2]//input");
    private By NextPageButton = By.xpath("//div[@class='office-form-navigation-container']//button");

    public ExpectationFormPage(WebDriver driver) { super(driver); }

    public int getNumberQuestions() {
        List<WebElement> list = driver.findElements(QuestionList);
        return driver.findElements(QuestionList).size();
    }

    public String getTitlePage() {
        return driver.findElement(Title).getText();
    }

    public void fillPage(String firstQuestionInput, int secondQuestionInput) {
        InputTextFieldWidget input = new InputTextFieldWidget(driver);
        OptionBoxWidget option = new OptionBoxWidget(driver);

        input.SetTextIntoInput(FirstQuestion, firstQuestionInput);
        option.setOptionFromBy(SecondQuestion, secondQuestionInput);
    }

    public ArrayList<String> getInputText() {
        ArrayList<String> Inputs = new ArrayList<>();

        Inputs.add(new InputTextFieldWidget(driver).GetTextInput(FirstQuestion));
        Inputs.add(new OptionBoxWidget(driver).getOptionChosen(SecondQuestion));
        return Inputs;
    }

    public ButtonWidget getNextPageButton() {
        return new ButtonWidget(driver, NextPageButton);
    }
}
