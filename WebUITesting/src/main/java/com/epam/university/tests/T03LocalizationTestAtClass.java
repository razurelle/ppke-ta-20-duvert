package com.epam.university.tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.hamcrest.CoreMatchers.containsString;

public class T03LocalizationTestAtClass extends TestBase {

	@Test
	public void findSportsBetTitle() {
        driver.get("http://sportbetting.azurewebsites.net/sportsbetting-web/login");

        // toDo: Use css selector to localize the title "Üdvözlünk a SportsBeten!"
        By cssSelector = By.cssSelector("body > header > span");

        String title = driver.findElement(cssSelector).getText();
        Assert.assertThat("Üdvözlünk a SportsBeten!", containsString(title));
	}

	@Test
	public void findHomePageLinks() {
        driver.get("http://sportbetting.azurewebsites.net/sportsbetting-web/login");

        // toDo: use cssSelector to find the "Login" link
        By loginLinkLocator = By.cssSelector("body > main > p > a:nth-child(1)");

        String loginLink = driver.findElement(loginLinkLocator).getText();
        Assert.assertEquals("Bejelentkezés", loginLink);
        
        // toDo: use XPath to find the "Registration" link
        By registerLinkLocator = By.xpath("/html/body/main/p/a[2]");

        String registerLink = driver.findElement(registerLinkLocator).getText();
        Assert.assertEquals("Regisztrálás", registerLink);
	}
}
