package com.epam.university.tests;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.emptyString;
import static org.hamcrest.Matchers.is;

public class T05EbayTestAtClass extends TestBase {
    protected String ebayUrl = "http://www.ebay.com";
    private String ebayAdvancedSearchUrl = "https://www.ebay.com/sch/ebayadvsearch";

    protected By categoryDropdownSelect = By.id("gh-cat");
    protected By searchButton = By.id("gh-btn");
    protected By searchTextInput = By.id("gh-ac");
    protected By saveThisSearchButton = By.xpath("//*[@id=\"s0-14-11-5-1[0]-71-6\"]/div/button/span");

    private final String searchTerm = "samsung";
    private final String consumerElectronicsCategory = "Consumer Electronics";

    /**
     * Purpose of this test is to verify the Search input field
     * 1) To test this field, type in the test string into the field and make sure it's displayed there.
     * 2) Then delete the string from the input box and make sure that the field is empty
     */
    @Test
    public void sendAndClearUsernameField() {
        navigateToPage(ebayUrl);
        // 1) get the locator for "searchTextInput"
        WebElement inputSearch = driver.findElement(searchTextInput);

        // 2) Type 'samsung' to search field
        inputSearch.sendKeys("samsung");

        Assert.assertEquals(searchTerm, inputSearch.getAttribute("value"));

        // 3) Clear search field
        inputSearch.clear();

        Assert.assertThat(inputSearch.getAttribute("value"), is(emptyString()));
    }

    /**
     * Purpose of this test is to test some of the Advanced Search capabilities.
     * 1) Verify that the "Title and description" checkbox can be checked
     * 2) Verify that the "From preferred location" radio button can be selected
     */
    @Test
    public void selectCheckBoxAndRadioButton() {
        navigateToPage(ebayAdvancedSearchUrl);
        WebElement checkBoxTitleAndDescription = driver.findElement(By.id("LH_TitleDesc"));
        // 1) Select checkbox 'Title and description'
        checkBoxTitleAndDescription.click();
        Assert.assertTrue(checkBoxTitleAndDescription.isSelected());

        WebElement fromPreferredLocationRadioButton = driver.findElement(By.id("LH_PrefLocRadio"));
        // 2) Select radio button 'From preferred location'
        fromPreferredLocationRadioButton.click();
        Assert.assertTrue(fromPreferredLocationRadioButton.isSelected());
    }

    /**
     * Purpose of this test is to verify that the basic search functionality works on eBay.
     * 1) Type in the "samsung" text to the search field
     * 2) Select the "Consumer Electronics" option from the Category drop down
     * 3) Click on the "Search" button
     */
    @Test
    public void searchForProduct() {
        navigateToPage(ebayUrl);

        // 1) Type in the "samsung" to the search field
        getElement(searchTextInput).sendKeys(searchTerm);

        // 2) Select the "Consumer Electronics" option from the Category drop down
        new Select(getElement(categoryDropdownSelect)).selectByValue("293");

        // 3) Click on the "Search" button
        getElement(searchButton).click();

        // 4) wait...
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Purpose of this test is to check the cookies used by eBay.
     * 1) Get all the cookies which eBay creates
     * 2) Display the cookie names on the console
     */
    @Test
    public void cookieTest() {
        navigateToPage(ebayUrl);

        // 1) Get all the available cookies
        Set<Cookie> cookies = driver.manage().getCookies();

        // Print the cookie names into the console
        cookies.forEach(cookie -> System.out.println(cookie.getName()));

        // 2) Make sure that we have at least 3 cookies available on ebay.com (assert)
        Assert.assertTrue(cookies.size()>=3);

        // 3) Delete all the cookies
        driver.manage().deleteAllCookies();

        // 4) Make sure that no cookies are available on ebay.com anymore
        cookies = driver.manage().getCookies();
        Assert.assertTrue(cookies.isEmpty());

    }

    /**
     * Purpose of this test is to verify that the Search result links to the correct product
     * 1) Search for the "samsung" products in "Consumer Electronics" category
     * 2) When the search results are displayed, get the _title_ of the second product from the list
     * 3) Open Product Details of this product (by Shift-click) in a new window
     * 4) Verify that the previously saved title (from step 2) is the same in the new Product Details window as well
     */
    @Test
    public void multipleWindow() {
        navigateToPage(ebayUrl);

        // Search for the "samsung" products
        getElement(searchTextInput).sendKeys(searchTerm);

        // Select the "Consumer Electronics" option from the Category drop down
        new Select(getElement(categoryDropdownSelect)).selectByVisibleText(consumerElectronicsCategory);

        // Click on the "Search" button
        getElement(searchButton).click();

        // Wait till the "Save this search" button becomes clickable
        wait.until(ExpectedConditions.elementToBeClickable(getElement(saveThisSearchButton)));

        // Get result links
        List<WebElement> resultLinks = driver.findElements(By.className("s-item__link"));

        // Locate the 2nd product in the list, get the product title
        scrollElementIntoView(resultLinks.get(2));
        String productTitle = resultLinks.get(2).getText();

        // Shift-click on the selected product
        new Actions(driver).keyDown(Keys.SHIFT).click(resultLinks.get(2)).keyUp(Keys.SHIFT).build().perform();

        // 1) Switch to the opened window and verify if the product title is the same on the product detail page
        switchToLastOpenedWindow();

        // 2) Get the product title (replace the "null" with the correct id)
        String productTitleInNewWindow = driver.findElement(By.id("itemTitle")).getText();
        Assert.assertEquals(productTitle, productTitleInNewWindow);

        driver.close();
    }

    @Test
    public void screenshotTest() throws IOException {
        navigateToPage(ebayUrl);

        // Get screenshot
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String screenshotPath = getScreenshotName();
        FileUtils.copyFile(screenshot, new File(screenshotPath));
        Assert.assertTrue(isSavedScreenshotFile(screenshotPath));
    }

    private boolean isSavedScreenshotFile(String screenshotPath) {
        File dir = new File("target/screenshots");
        File[] matchingFiles = dir.listFiles((dir1, name) -> screenshotPath.contains(name));
        return matchingFiles != null && matchingFiles.length != 0;
    }

    private String getScreenshotName() {
        StackTraceElement stack = Thread.currentThread().getStackTrace()[2];
        String[] classArray = stack.getClassName().split("\\.");
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss");
        Date curDate = new Date();
        String strDate = sdf.format(curDate);
        return "target/screenshots/" + classArray[(classArray.length - 1)] + "_" + stack.getMethodName() + "_" + strDate
                + "_error.jpg";
    }

    protected WebElement getElement(By element) {
        return driver.findElement(element);
    }

    protected void navigateToPage(String url) {
        driver.get(url);
    }

    private void scrollElementIntoView(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    /**
     * switch to the last opened window
     */
    private void switchToLastOpenedWindow() {
        // 1) get the available window handlers

        // 2) Switch to last opened window
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

    }

}
