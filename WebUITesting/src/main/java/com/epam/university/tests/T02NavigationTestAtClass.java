package com.epam.university.tests;

import org.junit.Assert;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class T02NavigationTestAtClass extends TestBase {
	
	private final String GOOGLE_URL = "http://www.google.com";
	private final String EPAM_URL = "http://www.epam.com";
	private final String SPORTS_BETTING = "http://sportbetting.azurewebsites.net/sportsbetting-web/";
	
	private String assertTitleChange(String oldTitle) {
		String newTitle = driver.getTitle();
		Assert.assertNotEquals(oldTitle, newTitle);
		return newTitle;
	}
	
	@Test
	public void testNavigation() throws MalformedURLException {
		final URL SPORTS_BETTING_URL_FORMAT = new URL(SPORTS_BETTING);
		String title = "";
		
		//Navigate to http://www.google.com -> GOOGLE_URL
		driver.get(GOOGLE_URL);
		title = assertTitleChange(title);

		//Navigate to http://www.epam.com
		driver.get(EPAM_URL);
		title = assertTitleChange(title);
        
		//Navigate to sportsbetting-web/login with url format -> SPORTS_BETTING_URL_FORMAT
		driver.navigate().to(SPORTS_BETTING_URL_FORMAT);
		title = assertTitleChange(title);
		
		//Navigate back
		driver.navigate().back();
		title = assertTitleChange(title);
		
		//Navigate forward
		driver.navigate().forward();
		title = assertTitleChange(title);
		
		//Refresh browser window
		driver.navigate().refresh();
		Assert.assertEquals(title, driver.getTitle());

		//Create an assert and check current URL
		Assert.assertEquals(SPORTS_BETTING, driver.getCurrentUrl());
    }
}
