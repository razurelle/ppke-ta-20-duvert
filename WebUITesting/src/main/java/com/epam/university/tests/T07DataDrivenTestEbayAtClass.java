package com.epam.university.tests;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

@RunWith(JUnitParamsRunner.class)
public class T07DataDrivenTestEbayAtClass extends T05EbayTestAtClass {

	/***
	 * 
	 * Description: test case to demonstrate DDT parameterized test case
	 * with @FileParameters annotation
	 * 
	 * @param searchKey
	 * @param category
	 */
	@Test
	@FileParameters("src/main/resources/tdd/test.csv")
	public void testLoginWithFileParameters(String searchKey, String category) {
		searchForProduct(searchKey, category);
	}

	/***
	 * 
	 * Description: test case to demonstrate DDT parameterized test case with data provider method
	 * with @FileParameters annotation
	 *
	 * @param searchKey
	 * @param category
	 */
	@Test
	@Parameters(method = "getTestData")
	public void testLoginWithParameterMethod(TestData data) {
		searchForProduct(data.getSearchKey(), data.getCategory());
	}

	public Object[] getTestData() {
		Object[] testDataArray = new Object[3];
		testDataArray[0] = new TestData("clean code", "Books");
		testDataArray[1] = new TestData("beatles", "Music");
		testDataArray[2] = new TestData("USA", "Travel");
		return testDataArray;
	}

	private class TestData {
		private String searchKey;
		private String category;

		TestData(String searchKey, String category) {
			this.searchKey = searchKey;
			this.category = category;
		}

		public String getSearchKey() {
			return searchKey;
		}

		public String getCategory() {
			return category;
		}
	}

	private void searchForProduct(String searchKey, String category) {
		navigateToPage(ebayUrl);
		getElement(searchTextInput).sendKeys(searchKey);
		new Select(getElement(categoryDropdownSelect)).selectByVisibleText(category);
		getElement(searchButton).click();
		wait.until(ExpectedConditions.elementToBeClickable(getElement(saveThisSearchButton)));
	}
}
