package com.epam.university.tests;

import com.epam.university.pagemodels.pages.ProductDetailsPage;
import com.epam.university.pagemodels.pages.SearchResultsPage;
import com.epam.university.pagemodels.widgets.SearchWidget;
import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RunWith(JUnitParamsRunner.class)
public class T13PageObjectTest extends TestBase {

    private final String searchTerm = "Samsung Galaxy J5";
    private final String consumerElectronicsCategory = "Consumer Electronics";

    /**
     * Search for the given product
     * Make sure that the search returns at least ONE product
     */
    @Test
    public void searchForMobileTest() throws IOException {
        SearchWidget searchWidget = new SearchWidget(driver);

        searchWidget.navigateToEbay(driver);

        searchWidget.searchForProduct(searchTerm);
        searchWidget.selectCategory(consumerElectronicsCategory);
        SearchResultsPage searchResultsPage = searchWidget.clickOnSearchButton();

        try {
            Assert.assertTrue("Save Search button should be displayed", searchResultsPage.isSaveSearchButtonDisplayed());
            Assert.assertTrue("We should find at least one product for: " + searchTerm, searchResultsPage.getNumberOfResults() >= 1);
        }
        catch (AssertionError assertionError) {
            this.takeScreenshot("SearchForMobile_Error");
        }
    }

    /**
     * Search for multiple products in different sections, from CSV
     * Make sure that at least ONE product is displayed in Search results
     * @param data
     */
    @Test
    @FileParameters("src/main/resources/tdd/test.csv")
    public void searchForMultipleProductsTest(String searchKey, String category) throws IOException {
        SearchWidget searchWidget = new SearchWidget(driver);

        searchWidget.navigateToEbay(driver);

        searchWidget.searchForProduct(searchKey);
        searchWidget.selectCategory(category);
        SearchResultsPage searchResultsPage = searchWidget.clickOnSearchButton();

        try {
            Assert.assertTrue("Save Search button should be displayed", searchResultsPage.isSaveSearchButtonDisplayed());
            Assert.assertTrue("We should find at least one product for: " + searchTerm, searchResultsPage.getNumberOfResults() >= 1);
        }
        catch (AssertionError assertionError) {
            this.takeScreenshot("SearchForMultipleProducts_ERROR");
        }
    }

    /**
     * Purpose of this test is to verify that the Search result links to the correct product
     * 1) Search for the "samsung" products in "Consumer Electronics" category
     * 2) When the search results are displayed, get the _title_ of the second product from the list
     * 3) Open Product Details of this product (by Shift-click) in a new window
     * 4) Verify that the previously saved title (from step 2) is the same in the new Product Details window as well
     */
    @Test
    public void multipleWindow() throws IOException {
        SearchWidget searchWidget = new SearchWidget(driver);

        searchWidget.navigateToEbay(driver);

        searchWidget.searchForProduct(searchTerm);
        searchWidget.selectCategory(consumerElectronicsCategory);
        SearchResultsPage searchResultsPage = searchWidget.clickOnSearchButton();

        Assert.assertTrue("Save Search button should be displayed", searchResultsPage.isSaveSearchButtonDisplayed());

        // Get result links
        List<WebElement> resultLinks = searchResultsPage.getResultLinks();

        // Locate the 2nd product in the list, get the product title
        String productTitle = resultLinks.get(1).getText();

        // Shift-click on the selected product
        new Actions(driver).keyDown(Keys.SHIFT).click(resultLinks.get(1)).keyUp(Keys.SHIFT).build().perform();

        // Switch to the opened window and verify if the product title is the same on the product detail page
        List<String> handlers = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(handlers.get(handlers.size()-1));

        // Get the product title (replace the "null" with the correct id)
        ProductDetailsPage productDetailsPage = new ProductDetailsPage(driver);
        String productTitleInNewWindow = productDetailsPage.getProductTitle();

        try {
            Assert.assertEquals(productTitle, productTitleInNewWindow);
        }
        catch (AssertionError assertionError) {
            this.takeScreenshot("MultipleWindow_ERROR");
        }
    }
}
