package com.epam.university.tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class T04ClickTestAtClass extends TestBase {
	
	private String SPORTS_BET_URL = "http://sportbetting.azurewebsites.net/sportsbetting-web/login";
	
	@Test
	public void testButtonClick() {
        driver.get(SPORTS_BET_URL);

        String originalTitle = driver.findElement(By.cssSelector("header>span")).getText();

        // toDo: locate the link and click on the "English" link in the top right corner
		By englishLinkLocator = By.cssSelector("body > header > div > a:nth-child(2)");
		driver.findElement(englishLinkLocator).click();

        String englishTitle = driver.findElement(By.cssSelector("header>span")).getText();
        Assert.assertNotEquals(originalTitle, englishTitle);

        // toDo: click on Registration link
		By registrationLinkLocator = By.xpath("/html/body/main/p/a[2]");
		driver.findElement(registrationLinkLocator).click();

        Assert.assertEquals("Player Registration", driver.getTitle());
	}
	
	@Test
	public void testModifiedClick() {
		driver.get(SPORTS_BET_URL);

		//toDo: Use Actions class to create a shift click on Register link
        //You have to see two browser windows after a successful run

		//toDo: your code here:
		By registrationLinkLocator = By.xpath("/html/body/main/p/a[2]");
		new Actions(driver).keyDown(Keys.SHIFT).click(driver.findElement(registrationLinkLocator))
				.keyUp(Keys.SHIFT).build().perform();

		Assert.assertEquals(2, driver.getWindowHandles().size());
	}
	
	@Test
	public void testDragAndDrop() {
		driver.get("https://jqueryui.com/resources/demos/droppable/default.html");

		//toDo: Locate the draggable element
        WebElement draggable = driver.findElement(By.xpath("//*[@id=\"draggable\"]"));

        //toDo: Locate the droppable element
        WebElement droppable = driver.findElement(By.xpath("//*[@id=\"droppable\"]"));

        //toDo: Add the drag and drop action here
		new Actions(driver).clickAndHold(draggable).moveToElement(droppable).release().build().perform();
        Assert.assertTrue(driver.findElement(By.cssSelector("#droppable > p:nth-child(1)")).getText().equals("Dropped!"));
	}
}
