package com.epam.university.tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;

public class T06WindowTestAtClass extends TestBase {

	@Test
	public void responsiveWindow() {
		driver.navigate().to("https://www.expedia.com/");

		// toDo: Maximize browser window
		driver.manage().window().maximize();
		Assert.assertTrue(driver.findElement(By.id("header-account-menu")).isDisplayed());
		Assert.assertFalse(driver.findElement(By.id("header-mobile-toggle")).isEnabled());

		// toDo: Set browser window size to 600x600
		driver.manage().window().setSize(new Dimension(600, 600));
		Assert.assertTrue(driver.findElement(By.id("header-mobile-toggle")).isDisplayed());
		Assert.assertFalse(driver.findElements(By.id("header-account-menu")).isEmpty());
	}

}
